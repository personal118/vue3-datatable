import { defineComponent as Z, onBeforeUnmount as Ye, onMounted as Ae, openBlock as c, createElementBlock as h, createElementVNode as i, withModifiers as K, normalizeClass as v, Fragment as F, createCommentVNode as y, ref as x, watch as z, createVNode as R, renderList as G, normalizeStyle as ze, createTextVNode as Be, toDisplayString as D, withDirectives as N, vModelText as X, vModelSelect as Ne, vShow as Je, useSlots as Qe, computed as I, unref as L, vModelCheckbox as Xe, renderSlot as Ze } from "vue";
const je = { class: "bh-filter-menu bh-absolute bh-z-[1] bh-bg-white bh-shadow-md bh-rounded-md bh-top-full bh-right-0 bh-w-32 bh-mt-1" }, et = ["onClick"], tt = /* @__PURE__ */ Z({
  __name: "column-filter",
  props: ["column"],
  emits: ["close", "filterChange"],
  setup(m, { emit: C }) {
    const a = m;
    Ye(() => {
      document.removeEventListener("click", n);
    }), Ae(() => {
      document.addEventListener("click", n);
    });
    const n = () => {
      C("close");
    }, b = (_) => {
      a.column.condition = _, _ === "" && (a.column.value = ""), C("filterChange", a.column);
    };
    return (_, l) => (c(), h("div", je, [
      i("div", {
        class: "bh-text-[13px] bh-font-normal bh-rounded bh-overflow-hidden",
        onClick: K(n, ["stop"])
      }, [
        i("button", {
          type: "button",
          class: v({ active: a.column.condition === "" }),
          onClick: l[0] || (l[0] = (o) => b(""))
        }, " No filter ", 2),
        a.column.type === "string" ? (c(), h(F, { key: 0 }, [
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "contain" }),
            onClick: l[1] || (l[1] = (o) => b("contain"))
          }, " Contain ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "not_contain" }),
            onClick: l[2] || (l[2] = (o) => b("not_contain"))
          }, " Not contain ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "equal" }),
            onClick: l[3] || (l[3] = (o) => b("equal"))
          }, " Equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "not_equal" }),
            onClick: l[4] || (l[4] = (o) => b("not_equal"))
          }, " Not equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "start_with" }),
            onClick: l[5] || (l[5] = (o) => b("start_with"))
          }, " Starts with ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "end_with" }),
            onClick: l[6] || (l[6] = (o) => b("end_with"))
          }, " Ends with ", 2)
        ], 64)) : a.column.type === "number" ? (c(), h(F, { key: 1 }, [
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "equal" }),
            onClick: l[7] || (l[7] = (o) => b("equal"))
          }, " Equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "not_equal" }),
            onClick: l[8] || (l[8] = (o) => b("not_equal"))
          }, " Not Equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "greater_than" }),
            onClick: l[9] || (l[9] = (o) => b("greater_than"))
          }, " Greater than ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "greater_than_equal" }),
            onClick: l[10] || (l[10] = (o) => b("greater_than_equal"))
          }, " Greater than or equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "less_than" }),
            onClick: l[11] || (l[11] = (o) => b("less_than"))
          }, " Less than ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "less_than_equal" }),
            onClick: l[12] || (l[12] = (o) => b("less_than_equal"))
          }, " Less than or equal ", 2)
        ], 64)) : a.column.type === "date" ? (c(), h(F, { key: 2 }, [
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "equal" }),
            onClick: l[13] || (l[13] = (o) => b("equal"))
          }, " Equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "not_equal" }),
            onClick: l[14] || (l[14] = (o) => b("not_equal"))
          }, " Not equal ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "greater_than" }),
            onClick: l[15] || (l[15] = (o) => b("greater_than"))
          }, " Greater than ", 2),
          i("button", {
            type: "button",
            class: v({ active: a.column.condition === "less_than" }),
            onClick: l[16] || (l[16] = (o) => b("less_than"))
          }, " Less than ", 2)
        ], 64)) : y("", !0),
        i("button", {
          type: "button",
          class: v({ active: a.column.condition === "is_null" }),
          onClick: l[17] || (l[17] = (o) => b("is_null"))
        }, " Is null ", 2),
        i("button", {
          type: "button",
          class: v({ active: a.column.condition === "is_not_null" }),
          onClick: l[18] || (l[18] = (o) => b("is_not_null"))
        }, " Not null ", 2)
      ], 8, et)
    ]));
  }
}), j = (m, C) => {
  const a = m.__vccOpts || m;
  for (const [n, b] of C)
    a[n] = b;
  return a;
}, lt = {}, nt = {
  version: "1.1",
  viewBox: "0 0 17 12",
  xmlns: "http://www.w3.org/2000/svg"
}, ot = /* @__PURE__ */ i("g", {
  fill: "none",
  "fill-rule": "evenodd"
}, [
  /* @__PURE__ */ i("g", {
    transform: "translate(-9 -11)",
    fill: "currentColor",
    "fill-rule": "nonzero"
  }, [
    /* @__PURE__ */ i("path", { d: "m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" })
  ])
], -1), st = [
  ot
];
function at(m, C) {
  return c(), h("svg", nt, st);
}
const Re = /* @__PURE__ */ j(lt, [["render", at]]), rt = {}, it = {
  viewBox: "0 0 24 24",
  width: "24",
  height: "24",
  stroke: "currentColor",
  "stroke-width": "3",
  fill: "none",
  "stroke-linecap": "round",
  "stroke-linejoin": "round",
  class: "css-i6dzq1"
}, ut = /* @__PURE__ */ i("line", {
  x1: "5",
  y1: "12",
  x2: "19",
  y2: "12"
}, null, -1), ct = [
  ut
];
function ht(m, C) {
  return c(), h("svg", it, ct);
}
const dt = /* @__PURE__ */ j(rt, [["render", ht]]), ft = {}, vt = {
  viewBox: "0 0 24 24",
  width: "24",
  height: "24",
  stroke: "currentColor",
  "stroke-width": "1.5",
  fill: "none",
  "stroke-linecap": "round",
  "stroke-linejoin": "round",
  class: "css-i6dzq1"
}, pt = /* @__PURE__ */ i("polygon", { points: "22 3 2 3 10 12.46 10 19 14 21 14 12.46 22 3" }, null, -1), bt = [
  pt
];
function gt(m, C) {
  return c(), h("svg", vt, bt);
}
const kt = /* @__PURE__ */ j(ft, [["render", gt]]), yt = { key: "hdrrow" }, Ct = { class: "bh-checkbox" }, mt = ["onClick"], wt = {
  width: "16",
  height: "16",
  viewBox: "0 0 14 14",
  fill: "none"
}, _t = {
  key: 0,
  class: "bh-filter bh-relative"
}, xt = ["onUpdate:modelValue"], $t = ["onUpdate:modelValue"], St = ["onUpdate:modelValue"], Ft = ["onUpdate:modelValue"], Lt = /* @__PURE__ */ i("option", { value: void 0 }, "All", -1), qt = /* @__PURE__ */ i("option", { value: !0 }, "True", -1), Mt = /* @__PURE__ */ i("option", { value: !1 }, "False", -1), At = [
  Lt,
  qt,
  Mt
], zt = ["onClick"], Me = /* @__PURE__ */ Z({
  __name: "column-header",
  props: [
    "all",
    "currentSortColumn",
    "currentSortDirection",
    "isOpenFilter",
    "isFooter",
    "checkAll"
  ],
  emits: [
    "selectAll",
    "sortChange",
    "filterChange",
    "toggleFilterMenu"
  ],
  setup(m, { emit: C }) {
    const a = m, n = x(null);
    return z(() => a.checkAll, () => {
      n.value && (n.value.indeterminate = a.checkAll !== 0 ? !a.checkAll : !1, n.value.checked = a.checkAll);
    }), (_, l) => (c(), h("tr", yt, [
      a.all.hasCheckbox ? (c(), h("th", {
        key: "chkall",
        class: v(["bh-w-px", {
          "bh-sticky bh-bg-[#f6f7fa] bh-z-[1]": a.all.stickyHeader || a.all.stickyFirstColumn,
          "bh-top-0": a.all.stickyHeader,
          "bh-left-0": a.all.stickyFirstColumn
        }])
      }, [
        i("div", Ct, [
          i("input", {
            ref_key: "selectedAll",
            ref: n,
            type: "checkbox",
            onClick: l[0] || (l[0] = K((o) => C("selectAll", o.target.checked), ["stop"]))
          }, null, 512),
          i("div", null, [
            R(Re, { class: "check" }),
            R(dt, { class: "intermediate" })
          ])
        ])
      ], 2)) : y("", !0),
      (c(!0), h(F, null, G(a.all.columns, (o, $) => (c(), h(F, null, [
        o.hide ? y("", !0) : (c(), h("th", {
          key: o.field,
          class: v(["bh-select-none bh-z-[1]", [
            a.all.sortable && o.sort ? "bh-cursor-pointer" : "",
            $ === 0 && a.all.stickyFirstColumn ? "bh-sticky bh-left-0 bh-bg-[#f6f7fa]" : "",
            a.all.hasCheckbox && $ === 0 && a.all.stickyFirstColumn ? "bh-left-[52px]" : ""
          ]]),
          style: ze({
            width: o.width,
            "min-width": o.minWidth,
            "max-width": o.maxWidth
          })
        }, [
          i("div", {
            class: v(["bh-flex bh-items-center", [o.headerClass ? o.headerClass : ""]]),
            onClick: (k) => a.all.sortable && o.sort && C("sortChange", o.field)
          }, [
            Be(D(o.title) + " ", 1),
            a.all.sortable && o.sort ? (c(), h("span", {
              key: 0,
              class: v(["bh-ml-3 bh-sort bh-flex bh-items-center", [a.currentSortColumn, a.currentSortDirection]])
            }, [
              (c(), h("svg", wt, [
                i("polygon", {
                  points: "3.11,6.25 10.89,6.25 7,1.75 ",
                  fill: "currentColor",
                  class: v(["bh-text-black/20", [
                    m.currentSortColumn === o.field && m.currentSortDirection === "asc" ? "!bh-text-primary" : ""
                  ]])
                }, null, 2),
                i("polygon", {
                  points: "7,12.25 10.89,7.75 3.11,7.75 ",
                  fill: "currentColor",
                  class: v(["bh-text-black/20", [
                    m.currentSortColumn === o.field && m.currentSortDirection === "desc" ? "!bh-text-primary" : ""
                  ]])
                }, null, 2)
              ]))
            ], 2)) : y("", !0)
          ], 10, mt),
          a.all.columnFilter && !a.isFooter ? (c(), h(F, { key: 0 }, [
            o.filter ? (c(), h("div", _t, [
              o.type === "string" ? N((c(), h("input", {
                key: 0,
                "onUpdate:modelValue": (k) => o.value = k,
                type: "text",
                class: "bh-form-control",
                onKeyup: l[1] || (l[1] = (k) => C("filterChange"))
              }, null, 40, xt)), [
                [
                  X,
                  o.value,
                  void 0,
                  { trim: !0 }
                ]
              ]) : y("", !0),
              o.type === "number" ? N((c(), h("input", {
                key: 1,
                "onUpdate:modelValue": (k) => o.value = k,
                type: "number",
                class: "bh-form-control",
                onKeyup: l[2] || (l[2] = (k) => C("filterChange"))
              }, null, 40, $t)), [
                [
                  X,
                  o.value,
                  void 0,
                  {
                    number: !0,
                    trim: !0
                  }
                ]
              ]) : o.type === "date" ? N((c(), h("input", {
                key: 2,
                "onUpdate:modelValue": (k) => o.value = k,
                type: "date",
                class: "bh-form-control",
                onChange: l[3] || (l[3] = (k) => C("filterChange"))
              }, null, 40, St)), [
                [X, o.value]
              ]) : o.type === "bool" ? N((c(), h("select", {
                key: 3,
                "onUpdate:modelValue": (k) => o.value = k,
                class: "bh-form-control",
                onChange: l[4] || (l[4] = (k) => C("filterChange")),
                onClick: l[5] || (l[5] = (k) => a.isOpenFilter = null)
              }, At, 40, Ft)), [
                [Ne, o.value]
              ]) : y("", !0),
              o.type !== "bool" ? (c(), h("button", {
                key: 4,
                type: "button",
                onClick: K((k) => C("toggleFilterMenu", o), ["stop"])
              }, [
                R(kt, { class: "bh-w-4" })
              ], 8, zt)) : y("", !0),
              N(R(tt, {
                column: o,
                type: o.type,
                onClose: l[6] || (l[6] = (k) => C("toggleFilterMenu", null)),
                onFilterChange: l[7] || (l[7] = (k) => C("filterChange"))
              }, null, 8, ["column", "type"]), [
                [Je, a.isOpenFilter === o.field]
              ])
            ])) : y("", !0)
          ], 64)) : y("", !0)
        ], 6))
      ], 64))), 256))
    ]));
  }
}), Bt = { class: "bh-antialiased bh-relative bh-text-black bh-text-sm bh-font-normal" }, Nt = ["onClick"], Rt = { class: "bh-checkbox" }, Pt = ["value"], Dt = ["innerHTML"], Ht = { key: 1 }, Tt = ["colspan"], Ot = {
  key: 0,
  class: "bh-absolute bh-inset-0 bh-bg-gray-200/50 bh-grid bh-place-content-center"
}, Ut = {
  xmlns: "http://www.w3.org/2000/svg",
  "xmlns:xlink": "http://www.w3.org/1999/xlink",
  style: { margin: "auto", background: "rgb(241, 242, 243)", display: "block" },
  width: "64",
  height: "64",
  viewBox: "0 0 100 100",
  preserveAspectRatio: "xMidYMid"
}, Vt = /* @__PURE__ */ i("path", {
  fill: "none",
  stroke: "#9ca3af",
  "stroke-width": "8",
  "stroke-dasharray": "42.76482137044271 42.76482137044271",
  d: "M24.3 30C11.4 30 5 43.3 5 50s6.4 20 19.3 20c19.3 0 32.1-40 51.4-40 C88.6 30 95 43.3 95 50s-6.4 20-19.3 20C56.4 70 43.6 30 24.3 30z",
  "stroke-linecap": "round",
  style: { transform: "scale(0.8)", "transform-origin": "50px 50px" }
}, [
  /* @__PURE__ */ i("animate", {
    attributeName: "stroke-dashoffset",
    repeatCount: "indefinite",
    dur: "1s",
    keyTimes: "0;1",
    values: "0;256.58892822265625"
  })
], -1), Et = [
  Vt
], It = {
  key: 0,
  class: "bh-pagination bh-py-5"
}, Gt = { class: "bh-flex bh-items-center bh-flex-wrap bh-flex-col sm:bh-flex-row bh-gap-4" }, Kt = { class: "bh-pagination-info bh-flex bh-items-center" }, Wt = { class: "bh-mr-2" }, Yt = ["value"], Jt = { class: "bh-pagination-number sm:bh-ml-auto bh-inline-flex bh-items-center bh-space-x-1" }, Qt = ["innerHTML"], Xt = {
  key: 1,
  "aria-hidden": "true",
  width: "14",
  height: "14",
  viewBox: "0 0 16 16"
}, Zt = /* @__PURE__ */ i("g", {
  fill: "currentColor",
  "fill-rule": "evenodd"
}, [
  /* @__PURE__ */ i("path", { d: "M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" }),
  /* @__PURE__ */ i("path", { d: "M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" })
], -1), jt = [
  Zt
], el = ["innerHTML"], tl = {
  key: 1,
  "aria-hidden": "true",
  width: "14",
  height: "14",
  viewBox: "0 0 16 16"
}, ll = /* @__PURE__ */ i("path", {
  fill: "currentColor",
  "fill-rule": "evenodd",
  d: "M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
}, null, -1), nl = [
  ll
], ol = ["onClick"], sl = ["innerHTML"], al = {
  key: 1,
  "aria-hidden": "true",
  width: "14",
  height: "14",
  viewBox: "0 0 16 16"
}, rl = /* @__PURE__ */ i("path", {
  fill: "currentColor",
  "fill-rule": "evenodd",
  d: "M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8L4.646 2.354a.5.5 0 0 1 0-.708z"
}, null, -1), il = [
  rl
], ul = ["innerHTML"], cl = {
  key: 1,
  "aria-hidden": "true",
  width: "14",
  height: "14",
  viewBox: "0 0 16 16"
}, hl = /* @__PURE__ */ i("g", {
  fill: "currentColor",
  "fill-rule": "evenodd"
}, [
  /* @__PURE__ */ i("path", { d: "M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8L3.646 2.354a.5.5 0 0 1 0-.708z" }),
  /* @__PURE__ */ i("path", { d: "M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8L7.646 2.354a.5.5 0 0 1 0-.708z" })
], -1), dl = [
  hl
], kl = /* @__PURE__ */ Z({
  __name: "custom-table",
  props: {
    loading: { type: Boolean, default: !1 },
    isStatic: { type: Boolean, default: !0 },
    skin: { default: "bh-table-striped bh-table-hover" },
    totalRows: { default: 0 },
    rows: { default: () => [] },
    columns: { default: () => [] },
    hasCheckbox: { type: Boolean, default: !1 },
    search: { default: "" },
    columnChooser: { type: Boolean, default: !1 },
    page: { default: 1 },
    pageSize: { default: 10 },
    pageSizeOptions: { default: () => [10, 20, 30, 50, 100] },
    showPageSize: { type: Boolean, default: !0 },
    rowClass: { default: [] },
    cellClass: { default: [] },
    sortable: { type: Boolean, default: !1 },
    sortColumn: { default: "id" },
    sortDirection: { default: "asc" },
    columnFilter: { type: Boolean, default: !1 },
    pagination: { type: Boolean, default: !0 },
    showNumbers: { type: Boolean, default: !0 },
    showNumbersCount: { default: 5 },
    showFirstPage: { type: Boolean, default: !0 },
    showLastPage: { type: Boolean, default: !0 },
    showPrev: { type: Boolean, default: !0 },
    showNext: { type: Boolean, default: !0 },
    firstArrow: { default: "" },
    lastArrow: { default: "" },
    nextArrow: { default: "" },
    previousArrow: { default: "" },
    paginationInfo: { default: "Showing {0} to {1} of {2} entries" },
    noDataContent: { default: "No data available" },
    stickyHeader: { type: Boolean, default: !1 },
    height: { default: "500px" },
    stickyFirstColumn: { type: Boolean, default: !1 },
    cloneHeaderInFooter: { type: Boolean, default: !1 },
    selectRowOnClick: { type: Boolean, default: !1 }
  },
  emits: [
    "sortChange",
    "searchChange",
    "pageChange",
    "pageSizeChange",
    "rowSelect",
    "filterChange",
    "rowClick",
    "rowDBClick"
  ],
  setup(m, { expose: C, emit: a }) {
    var ce;
    const n = m, b = Qe();
    for (const e of n.columns || []) {
      const s = ((ce = e.type) == null ? void 0 : ce.toLowerCase()) || "string";
      e.type = s, e.isUnique = e.isUnique !== void 0 ? e.isUnique : !1, e.hide = e.hide !== void 0 ? e.hide : !1, e.filter = e.filter !== void 0 ? e.filter : !0, e.search = e.search !== void 0 ? e.search : !0, e.sort = e.sort !== void 0 ? e.sort : !0, e.html = e.html !== void 0 ? e.html : !1, e.condition = !s || s === "string" ? "contain" : "equal";
    }
    const _ = x([]), l = x(n.page), o = x(
      n.pagination ? n.pageSize : n.rows.length
    ), $ = x(n.sortColumn), k = x(n.sortDirection), B = x(n.totalRows), S = x([]), Q = x(null), H = x(n.loading), T = x(n.search), Pe = n.columns, P = x(null), ee = x(null);
    let O = x(0);
    const De = x(230);
    Ae(() => {
      A();
    }), C({
      reset() {
        Ie();
      },
      getSelectedRows() {
        return Ge();
      },
      getColumnFilters() {
        return Ke();
      },
      clearSelectedRows() {
        return We();
      },
      selectRow(e) {
        ie(e);
      },
      unselectRow(e) {
        ue(e);
      },
      isRowSelected(e) {
        return Y(e);
      }
    });
    const He = (e, ...s) => e.replace(/{(\d+)}/g, (u, p) => typeof s[p] < "u" ? s[p] : u), w = I(() => {
      const e = n.columns.find((s) => s.isUnique);
      return (e == null ? void 0 : e.field) || null;
    }), M = I(() => {
      const e = o.value < 1 ? 1 : Math.ceil(B.value / o.value);
      return Math.max(e || 0, 1);
    }), te = I(() => (l.value - 1) * o.value + 1), le = I(() => {
      const e = l.value * o.value;
      return B.value >= e ? e : B.value;
    }), Te = I(() => {
      let e, s;
      return typeof n.showNumbersCount < "u" && n.showNumbersCount < M.value ? (e = Math.max(
        l.value - Math.floor(n.showNumbersCount / 2),
        1
      ), s = e + n.showNumbersCount - 1, s > M.value && (s = M.value, e = s - n.showNumbersCount + 1)) : (e = 1, s = M.value), Array.from(Array(s + 1 - e).keys()).map(
        (g) => e + g
      );
    }), A = () => {
      var U, V, E;
      H.value = !0;
      let e = n.rows || [];
      if ((U = n.columns) == null || U.forEach((t) => {
        t.filter && (t.value !== void 0 && t.value !== null && t.value !== "" || t.condition === "is_null" || t.condition == "is_not_null") && (t.type === "string" ? t.condition === "contain" ? e = e.filter((r) => {
          var d;
          return (d = r[t.field]) == null ? void 0 : d.toString().toLowerCase().includes(t.value.toLowerCase());
        }) : t.condition === "not_contain" ? e = e.filter((r) => {
          var d;
          return !((d = r[t.field]) != null && d.toString().toLowerCase().includes(t.value.toLowerCase()));
        }) : t.condition === "equal" ? e = e.filter((r) => {
          var d;
          return ((d = r[t.field]) == null ? void 0 : d.toString().toLowerCase()) === t.value.toLowerCase();
        }) : t.condition === "not_equal" ? e = e.filter((r) => {
          var d;
          return ((d = r[t.field]) == null ? void 0 : d.toString().toLowerCase()) !== t.value.toLowerCase();
        }) : t.condition == "start_with" ? e = e.filter((r) => {
          var d;
          return ((d = r[t.field]) == null ? void 0 : d.toString().toLowerCase().indexOf(t.value.toLowerCase())) === 0;
        }) : t.condition == "end_with" && (e = e.filter((r) => {
          var d;
          return ((d = r[t.field]) == null ? void 0 : d.toString().toLowerCase().substr(t.value.length * -1)) === t.value.toLowerCase();
        })) : t.type === "number" ? t.condition === "equal" ? e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) === parseFloat(t.value)) : t.condition === "not_equal" ? e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) !== parseFloat(t.value)) : t.condition === "greater_than" ? e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) > parseFloat(t.value)) : t.condition === "greater_than_equal" ? e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) >= parseFloat(t.value)) : t.condition === "less_than" ? e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) < parseFloat(t.value)) : t.condition === "less_than_equal" && (e = e.filter((r) => r[t.field] && parseFloat(r[t.field]) <= parseFloat(t.value))) : t.type === "date" ? t.condition === "equal" ? e = e.filter((r) => r[t.field] && W(r[t.field]) === t.value) : t.condition === "not_equal" ? e = e.filter((r) => r[t.field] && W(r[t.field]) !== t.value) : t.condition === "greater_than" ? e = e.filter((r) => r[t.field] && W(r[t.field]) > t.value) : t.condition === "less_than" && (e = e.filter((r) => r[t.field] && W(r[t.field]) < t.value)) : t.type === "bool" && (e = e.filter((r) => r[t.field] === t.value)), t.condition === "is_null" ? (e = e.filter((r) => r[t.field] == null || r[t.field] == ""), t.value = "") : t.condition === "is_not_null" && (t.value = "", e = e.filter((r) => r[t.field])));
      }), T.value && e.length) {
        let t = [];
        const r = (n.columns || []).filter((d) => d.search && !d.hide).map((d) => d.field);
        for (var s = 0; s < e.length; s++)
          for (var u = 0; u < r.length; u++)
            if ((V = re(e[s], r[u])) != null && V.toString().toLowerCase().includes(T.value.toLowerCase())) {
              t.push(e[s]);
              break;
            }
        e = t;
      }
      var p = new Intl.Collator(void 0, {
        numeric: !0,
        sensitivity: "base"
      });
      const g = k.value === "desc" ? -1 : 1, f = (E = $ == null ? void 0 : $.value) == null ? void 0 : E.split(".");
      e.sort((t, r) => {
        var d, he, de, fe, ve, pe, be, ge, ke, ye, Ce, me, we, _e, xe, $e, Se, Fe, Le, qe;
        return f.length === 2 ? p.compare((d = t[f[0]]) == null ? void 0 : d[f[1]], (he = r[f[0]]) == null ? void 0 : he[f[1]]) * g : f.length === 3 ? p.compare(
          (fe = (de = t[f[0]]) == null ? void 0 : de[f[1]]) == null ? void 0 : fe[f[2]],
          (pe = (ve = r[f[0]]) == null ? void 0 : ve[f[1]]) == null ? void 0 : pe[f[2]]
        ) * g : f.length === 4 ? p.compare(
          (ke = (ge = (be = t[f[0]]) == null ? void 0 : be[f[1]]) == null ? void 0 : ge[f[2]]) == null ? void 0 : ke[f[3]],
          (me = (Ce = (ye = r[f[0]]) == null ? void 0 : ye[f[1]]) == null ? void 0 : Ce[f[2]]) == null ? void 0 : me[f[3]]
        ) * g : f.length === 5 ? p.compare(
          ($e = (xe = (_e = (we = t[f[0]]) == null ? void 0 : we[f[1]]) == null ? void 0 : _e[f[2]]) == null ? void 0 : xe[f[3]]) == null ? void 0 : $e[f[4]],
          (qe = (Le = (Fe = (Se = r[f[0]]) == null ? void 0 : Se[f[1]]) == null ? void 0 : Fe[f[2]]) == null ? void 0 : Le[f[3]]) == null ? void 0 : qe[f[4]]
        ) * g : p.compare(
          t[$.value],
          r[$.value]
        ) * g;
      }), B.value = e.length || 0;
      const J = e.slice(te.value - 1, le.value);
      _.value = J || [], H.value = !1;
    };
    z(
      () => n.loading,
      () => {
        H.value = n.loading;
      }
    );
    const ne = (e) => {
      e ? P.value === e.field ? P.value = null : P.value = e.field : P.value = null;
    }, Oe = () => {
      if (l.value == 1)
        return !1;
      l.value--;
    }, Ue = (e) => {
      l.value = e;
    }, Ve = () => {
      if (l.value >= M.value)
        return !1;
      l.value++;
    };
    z(() => l.value, () => {
      q(!1), A(), a("pageChange", l.value);
    }), z(() => n.rows, () => {
      l.value = 1, q(!1), A();
    }), z(() => o.value, () => {
      l.value = 1, q(!1), A(), a("pageSizeChange", o.value);
    });
    const oe = (e) => {
      let s = "asc";
      e == $.value && k.value === "asc" && (s = "desc");
      let u = (l.value - 1) * o.value, p = o.value;
      $.value = e, k.value = s, q(!1), A(), a("sortChange", { offset: u, limit: p, field: e, direction: s });
    }, se = (e) => {
      Q.value = e.length && _.value.length && e.length === _.value.length;
      const s = _.value.filter(
        (u, p) => S.value.includes(w.value ? u[w.value] : p)
      );
      a("rowSelect", s);
    };
    z(() => S.value, se);
    const q = (e) => {
      e ? S.value = _.value.map(
        (s, u) => w.value ? s[w.value] : u
      ) : S.value = [];
    }, ae = () => {
      l.value = 1, q(!1), A(), a("filterChange", n.columns);
    };
    z(() => n.search, () => {
      l.value = 1, q(!1), A(), a("searchChange", T.value);
    }), z(
      () => n.search,
      () => {
        T.value = n.search;
      }
    );
    const re = (e, s) => {
      var u, p, g, f, J, U, V, E, t, r;
      if (s.includes(".")) {
        const d = s.split(".");
        return d.length === 5 ? (f = (g = (p = (u = e[d[0]]) == null ? void 0 : u[d[1]]) == null ? void 0 : p[d[2]]) == null ? void 0 : g[d[3]]) == null ? void 0 : f[d[4]] : d.length === 4 ? (V = (U = (J = e[d[0]]) == null ? void 0 : J[d[1]]) == null ? void 0 : U[d[2]]) == null ? void 0 : V[d[3]] : d.length === 3 ? (t = (E = e[d[0]]) == null ? void 0 : E[d[1]]) == null ? void 0 : t[d[2]] : (r = e[d[0]]) == null ? void 0 : r[d[1]];
      }
      return e == null ? void 0 : e[s];
    }, W = (e) => {
      try {
        if (!e)
          return "";
        const s = new Date(e), u = s.getDate(), p = s.getMonth() + 1;
        return s.getFullYear() + "-" + (p > 9 ? p : "0" + p) + "-" + (u > 9 ? u : "0" + u);
      } catch {
      }
      return "";
    }, Ee = (e, s) => {
      O.value++, O.value === 1 ? ee.value = setTimeout(() => {
        O.value = 0, n.selectRowOnClick && (Y(s) ? ue(s) : ie(s), se(S.value)), a("rowClick", e);
      }, De.value) : O.value === 2 && (clearTimeout(ee.value), O.value = 0, a("rowDBClick", e));
    }, Ie = () => {
      var e;
      (e = n.columns) == null || e.forEach((s, u) => {
        Pe[u];
      }), T.value = "", l.value = 1, $.value = "id", k.value = "asc", q(!1), A();
    }, Ge = () => _.value.filter(
      (s, u) => S.value.includes(w.value ? s[w.value] : u)
    ), Ke = () => n.columns, We = () => {
      S.value = [];
    }, ie = (e) => {
      if (!Y(e)) {
        const s = _.value.find((u, p) => p === e);
        S.value.push(
          w.value ? s[w.value] : e
        );
      }
    }, ue = (e) => {
      if (Y(e)) {
        const s = _.value.find((u, p) => p === e);
        S.value = S.value.filter(
          (u) => u !== (w.value ? s[w.value] : e)
        );
      }
    }, Y = (e) => {
      const s = _.value.find((u, p) => p === e);
      return s ? S.value.includes(
        w.value ? s[w.value] : e
      ) : !1;
    };
    return (e, s) => (c(), h("div", Bt, [
      i("div", {
        class: v(["bh-table-responsive", { "bh-min-h-[300px]": H.value }]),
        style: ze({ height: n.stickyHeader && n.height })
      }, [
        i("table", {
          class: v([n.skin])
        }, [
          i("thead", {
            class: v({ "bh-sticky bh-top-0 bh-z-10": n.stickyHeader })
          }, [
            R(Me, {
              all: n,
              currentSortColumn: $.value,
              currentSortDirection: k.value,
              isOpenFilter: P.value,
              checkAll: Q.value,
              onSelectAll: q,
              onSortChange: oe,
              onFilterChange: ae,
              onToggleFilterMenu: ne
            }, null, 8, ["currentSortColumn", "currentSortDirection", "isOpenFilter", "checkAll"])
          ], 2),
          i("tbody", null, [
            B.value ? (c(!0), h(F, { key: 0 }, G(_.value, (u, p) => (c(), h("tr", {
              key: u[L(w)] ? u[L(w)] : p,
              class: v([
                typeof n.rowClass == "function" ? m.rowClass(u) : n.rowClass,
                n.selectRowOnClick ? "bh-cursor-pointer" : ""
              ]),
              onClick: K((g) => Ee(u, p), ["prevent"])
            }, [
              n.hasCheckbox ? (c(), h("td", {
                key: 0,
                class: v({
                  "bh-sticky bh-left-0 bh-bg-[#f6f7fa]": n.stickyFirstColumn
                })
              }, [
                i("div", Rt, [
                  N(i("input", {
                    "onUpdate:modelValue": s[0] || (s[0] = (g) => S.value = g),
                    type: "checkbox",
                    value: u[L(w)] ? u[L(w)] : p,
                    onClick: s[1] || (s[1] = K(() => {
                    }, ["stop"]))
                  }, null, 8, Pt), [
                    [Xe, S.value]
                  ]),
                  i("div", null, [
                    R(Re, { class: "check" })
                  ])
                ])
              ], 2)) : y("", !0),
              (c(!0), h(F, null, G(n.columns, (g, f) => (c(), h(F, null, [
                g.hide ? y("", !0) : (c(), h("td", {
                  key: g.field,
                  class: v([
                    typeof n.cellClass == "function" ? m.cellClass(u) : n.cellClass,
                    f === 0 && n.stickyFirstColumn ? "bh-sticky bh-left-0 bh-bg-[#f6f7fa]" : "",
                    n.hasCheckbox && f === 0 && n.stickyFirstColumn ? "bh-left-[52px]" : "",
                    g.cellClass ? g.cellClass : ""
                  ])
                }, [
                  L(b)[g.field] ? Ze(e.$slots, g.field, {
                    key: 0,
                    value: u
                  }) : g.cellRenderer ? (c(), h("div", {
                    key: 1,
                    innerHTML: g.cellRenderer(u)
                  }, null, 8, Dt)) : (c(), h(F, { key: 2 }, [
                    Be(D(re(u, g.field)), 1)
                  ], 64))
                ], 2))
              ], 64))), 256))
            ], 10, Nt))), 128)) : (c(), h("tr", Ht, [
              i("td", {
                colspan: n.columns.length + 1
              }, D(n.noDataContent), 9, Tt)
            ]))
          ]),
          n.cloneHeaderInFooter ? (c(), h("tfoot", {
            key: 0,
            class: v({ "bh-sticky bh-bottom-0": n.stickyHeader })
          }, [
            R(Me, {
              all: n,
              currentSortColumn: $.value,
              currentSortDirection: k.value,
              isOpenFilter: P.value,
              isFooter: !0,
              checkAll: Q.value,
              onSelectAll: q,
              onSortChange: oe,
              onFilterChange: ae,
              onToggleFilterMenu: ne
            }, null, 8, ["currentSortColumn", "currentSortDirection", "isOpenFilter", "checkAll"])
          ], 2)) : y("", !0)
        ], 2),
        H.value ? (c(), h("div", Ot, [
          (c(), h("svg", Ut, Et))
        ])) : y("", !0)
      ], 6),
      n.pagination ? (c(), h("div", It, [
        i("div", Gt, [
          i("div", Kt, [
            i("span", Wt, D(He(
              n.paginationInfo,
              B.value ? L(te) : 0,
              L(le),
              B.value
            )), 1),
            n.showPageSize ? N((c(), h("select", {
              key: 0,
              "onUpdate:modelValue": s[2] || (s[2] = (u) => o.value = u),
              class: "bh-pagesize"
            }, [
              (c(!0), h(F, null, G(n.pageSizeOptions, (u) => (c(), h("option", {
                value: u,
                key: u
              }, D(u), 9, Yt))), 128))
            ], 512)), [
              [Ne, o.value]
            ]) : y("", !0)
          ]),
          i("div", Jt, [
            n.showFirstPage ? (c(), h("button", {
              key: 0,
              type: "button",
              class: v(["bh-page-item first-page", { disabled: l.value <= 1 }]),
              onClick: s[3] || (s[3] = (u) => l.value = 1)
            }, [
              n.firstArrow ? (c(), h("span", {
                key: 0,
                innerHTML: n.firstArrow
              }, null, 8, Qt)) : (c(), h("svg", Xt, jt))
            ], 2)) : y("", !0),
            n.showPrev ? (c(), h("button", {
              key: 1,
              type: "button",
              class: v(["bh-page-item previous-page", { disabled: l.value <= 1 }]),
              onClick: Oe
            }, [
              n.previousArrow ? (c(), h("span", {
                key: 0,
                innerHTML: n.previousArrow
              }, null, 8, el)) : (c(), h("svg", tl, nl))
            ], 2)) : y("", !0),
            n.showNumbers ? (c(!0), h(F, { key: 2 }, G(L(Te), (u) => (c(), h("button", {
              key: u,
              type: "button",
              class: v(["bh-page-item", {
                disabled: l.value === u,
                "bh-active": u === l.value
              }]),
              onClick: (p) => Ue(u)
            }, D(u), 11, ol))), 128)) : y("", !0),
            n.showNext ? (c(), h("button", {
              key: 3,
              type: "button",
              class: v(["bh-page-item next-page", { disabled: l.value >= L(M) }]),
              onClick: Ve
            }, [
              n.nextArrow ? (c(), h("span", {
                key: 0,
                innerHTML: n.nextArrow
              }, null, 8, sl)) : (c(), h("svg", al, il))
            ], 2)) : y("", !0),
            n.showLastPage ? (c(), h("button", {
              key: 4,
              type: "button",
              class: v(["bh-page-item last-page", { disabled: l.value >= L(M) }]),
              onClick: s[4] || (s[4] = (u) => l.value = L(M))
            }, [
              n.lastArrow ? (c(), h("span", {
                key: 0,
                innerHTML: n.lastArrow
              }, null, 8, ul)) : (c(), h("svg", cl, dl))
            ], 2)) : y("", !0)
          ])
        ])
      ])) : y("", !0)
    ]));
  }
});
export {
  kl as default
};
